create table transactions (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
date date,
payer int(5),
payer_method varchar(10),
payee int(5),
payee_method varchar(10),
amount float
complete varchar(3) default "No"
) ENGINE=MyISAM;
